var Main = {};

Main.onLoad = function() {
	"use strict";
	 if (window.tizen === undefined) {
       
        setTimeout(
        		function(){
        			 Main.log('This application needs to be run on Tizen device');
        		},
        		1000
        );
    }else{
        setTimeout(function() { console.log('La aplicacion ha iniciado'); }, 11000);
    }
};

// 'documents', 'music', 'images', 'videos', 'downloads'
// Main.createFolder('documents','nuevo');
// Main.changeDir('documents/nuevo');

Main.listDir = function(dir) {
	var documentsDir;
	function onsuccess(files) {
		if (files.length >= 1) {
			for (var i = 0; i < files.length; i++) {
			    console.log("El nombre del archivo es  " + files[i].name); // displays file name
			 }
		}
		else{
			console.log("La carpeta esta vacia");
		}
		  
	}

	function onerror(error) {
	  console.log("The error " + error.message + " occurred when listing the files in the selected folder");
	}

	tizen.filesystem.resolve(
	  dir,
	  function(dir) {
	    documentsDir = dir;
	    dir.listFiles(onsuccess, onerror);
	  }, function(e) {
	    console.log("Error" + e.message);
	  }, "rw"
	);
};

Main.createFolder = function(mainfolder,nameFolder) {
	var documentsDir;
	function onsuccess(files) {		
	
	}

	function onerror(error) {
	  console.log("The error " + error.message + " occurred when listing the files in the selected folder");
	}

	tizen.filesystem.resolve(
	  mainfolder,
	  function(dir) {
	    documentsDir = dir;
	    dir.createDirectory(nameFolder);
	    console.log("la carpeta "+ nameFolder + " ha sido creada")
	  }, function(e) {
	    console.log("Error" + e.message);
	  }, "rw"
	);
}

Main.deleteFolder = function(nameFolder) {
	var documentsDir;
	function onsuccess(files) {
	  for (var i = 0; i < files.length; i++) {
	    if (files[i].isDirectory) {
	      documentsDir.deleteDirectory(
	          files[i].fullPath,
	          false,
	          function() {
	            console.log("Directory Deleted");
	          }, function(e) {
	            console.log("Error" + e.message);
	          });
	    }
	  }
	}

	function onerror(error) {
	  console.log("The error " + error.message + " occurred when listing the files in the selected folder");
	}

	tizen.filesystem.resolve(
	    nameFolder,
	    function(dir) {
	      documentsDir = dir;
	      dir.listFiles(onsuccess,onerror);
	    }, function(e) {
	      console.log("Error" + e.message);
	    }, "rw"
	);
}

Main.createFile = function(folderName, fileName) {
	var documentsDir;
	function onsuccess(files) {
		var testFile = documentsDir.createFile(fileName);
		console.log("El archivo " + fileName + "ha sido creado");
	}

	function onerror(error) {
	  console.log("The error " + error.message + " occurred when listing the files in the selected folder");
	}

	tizen.filesystem.resolve(
	    folderName,
	    function(dir) {
	      documentsDir = dir; dir.listFiles(onsuccess,onerror);
	    }, function(e) {
	      console.log("Error" + e.message);
	    }, "rw"
	);
}

Main.deleteFile = function(folderName, fileName) {
	function onsuccess(files) {
			var path = folderName+"/"+fileName;
		      documentsDir.deleteFile(
		          path,
		          function() {
		            console.log("File Deleted");
		          }, function(e) {
		            console.log("Error" + e.message);
		          });
		}

		function onerror(error) {
		  console.log("The error " + error.message + " occurred when listing the files in the selected folder");
		}

		var documentsDir;
		tizen.filesystem.resolve(
		  folderName,
		  function(dir) {
		    documentsDir = dir;
		    dir.listFiles(onsuccess,onerror);
		  }, function(e) {
		    console.log("Error" + e.message);
		  }, "rw"
		);
}

Main.readFile = function(folderName,fileName) {
	function onsuccess(files) {
		  for (var i = 0; i < files.length; i++) {
		    console.log("File Name is " + files[i].name); // displays file name
		    if (files[i].isDirectory == false)
		      files[i].readAsText(
		          function(str) {
		            console.log("The file content " + str);
		          }, function(e) {
		            console.log("Error " + e.message);
		          }, "UTF-8"
		      );
		  }
		}

		function onerror(error) {
		  console.log("The error " + error.message + " occurred when listing the files in the selected folder");
		}

		var documentsDir;
		tizen.filesystem.resolve(
		    folderName,
		    function(dir) {
		      documentsDir = dir;
		      dir.listFiles(onsuccess,onerror);
		    }, function(e) {
		      console.log("Error" + e.message);
		    }, "rw"
		);
}

Main.writeFile = function(folderName,fileName) {
	var documentsDir;
	function onsuccess(files) {
	  for (var i = 0; i < files.length; i++) {
	    console.log("File Name is " + files[i].name); // displays file name
	  }

	  var testFile = documentsDir.createFile("test3.txt");

	  if (testFile != null) {
	    testFile.openStream(
	      "w",
	      function(fs) {
	        fs.write("HelloWorld");
	        fs.close();
	      }, function(e) {
	        console.log("Error " + e.message);
	      }, "UTF-8"
	    );
	  }
	}

	function onerror(error) {
	  console.log("The error " + error.message + " occurred when listing the files in the selected folder");
	}

	tizen.filesystem.resolve(
	  'documents',
	  function(dir) {
	    documentsDir = dir;
	    dir.listFiles(onsuccess, onerror);
	  }, function(e) {
	    console.log("Error" + e.message);
	  }, "rw"
	);
}